# https://www.vivien-chevallier.com/Articles/deploying-a-web-certificate-with-arm-template-to-an-azure-resource-group-and-retrieving-certificate-thumbprint
Param([string] $pfxFilePath)
$filename = Split-Path -Path $pfxFilePath -Leaf -Resolve

$pfxFileBytes = get-content $pfxFilePath -Encoding Byte

[System.Convert]::ToBase64String($pfxFileBytes) | Out-File "${filename}.Base64.txt"
