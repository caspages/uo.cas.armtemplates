# Select the CASIT Web Services Dev subscription
Set-AzContext -Subscription "CASIT Web Services Dev"

### 
### Get Certificates
### ================
# Get all the certificates in currently selected subscription
Get-AzResource -ResourceType Microsoft.Web/certificates | ft
# Get all certificates in the resource group 
Get-AzWebAppCertificate -ResourceGroupName "cascommon-dev-rg"

###
### Remove Resource
### ===============
# This command fails! "ResourceNotFound : The Resource 'Microsoft.Web/certificates/duckling4vppu2jtx5uok' under resource group '<null>' was not found."
# DON'T USE -- Remove-AzResource -ResourceName duckling4vppu2jtx5uok -ResourceType Microsoft.Web/certificates

# Works when we provide the resource group (y)
Remove-AzResource -ResourceGroupName cascommon-dev-rg -ResourceName duckling4vppu2jtx5uok -ResourceType Microsoft.Web/certificates

##
## Template Commands
## =========================
# Test deployment of a template
Test-AzureRmResourceGroupDeployment -ResourceGroupName duckling-dev-rg -Mode Incramental -TemplateFile .armtemplates/azureDeploy.json -TemplateParameterFile .armtemplates/azureDeployParameters.json 
