Overview
========
These are Azure Resource Manager (ARM) templates for deploying web applications.

For further information, refer to technical documentation: https://casdocs.uoregon.edu/display/WEBDEV/UO.CAS.ARMTemplates+Technical+Documentation

Notes
=====
Tried running the azureDeploySSLCertificate.json as a deployment today. 
- it seemed to work well
- afterwards, the certificate was visible in the duckling-dev-rg resource group (using powershell script to look it up)
  - the certificate looked identical to the certificate in the web-services-sandbox-rg resource group
Tried running the azureDeployHostname deployment next, but that fails with 'certificate not found' error
Got the same error when trying to do the "Add SSL Binding" workflow in portal.azure.com
- Hostname could be selected, as expected
- The certificate thumbrint showed no certificate...
Tried adding the certificate using the mainstream interface, that Daniel recommends in documentation
- certificate uploads nicely
- able to find the certifciate in the SSL bindings menu
Tried deleting the SSL Binding
- After around 2 minutes, the site was no longer accessible, again
Tried running the azureDeployHostname deployment next
- Success! It works! Hooray!
- The SSL Binding is updated. So something is going wrong in the certificate uploading step...
  - The only difference is Name and ID
  - non-working: 
    Id: /subscriptions/d88e0c85-6145-437a-99a3-64e8900098a7/resourceGroups/duckling-dev-rg/providers/Microsoft.Web/certificates/duckling-SSLCert-32cirvrobc7ra
    Name: duckling-SSLCert-32cirvrobc7ra
  - working:
    Id: /subscriptions/d88e0c85-6145-437a-99a3-64e8900098a7/resourceGroups/duckling-dev-rg/providers/Microsoft.Web/certificates/87C44285CFE6BAA173867832AA9F8FD9069F15D6-cascommon-dev-rg-WestUS2webspace
    Name: 87C44285CFE6BAA173867832AA9F8FD9069F15D6-cascommon-dev-rg-WestUS2webspace
  - So maybe the name of the certificate is really important? <thumbprint>-<resourceGroupContainingASP>-<location>webspace
    - it is not trivial to name the certificate using the thumbprint... because normally we get the thumbprint by referencing the created certificate... lol
    



Resources
==========
* Azure Resource Manager template overview: https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-authoring-templates
* Deploying an ARM template via Azure Portal: https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-manager-quickstart-create-templates-use-the-portal
* Sample ARM templates: https://azure.microsoft.com/en-us/resources/templates/
